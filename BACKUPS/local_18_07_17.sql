-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla db_betsson.apuesta
CREATE TABLE IF NOT EXISTS `apuesta` (
  `idApuesta` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  `idPartido` int(11) NOT NULL,
  `ganancia` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idApuesta`),
  KEY `fk_apuesta_usuario1_idx` (`idUsuario`),
  KEY `fk_apuesta_partido1_idx` (`idPartido`),
  CONSTRAINT `fk_apuesta_partido1` FOREIGN KEY (`idPartido`) REFERENCES `partido` (`idPartido`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_apuesta_usuario1` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.apuesta: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `apuesta` DISABLE KEYS */;
/*!40000 ALTER TABLE `apuesta` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.equipo
CREATE TABLE IF NOT EXISTS `equipo` (
  `idEquipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idEquipo`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.equipo: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` (`idEquipo`, `nombre`, `created_at`, `updated_at`) VALUES
	(222, 'IFK Norrköping', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(223, 'BK Häcken', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(224, 'GIF Sundsvall', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(225, 'AIK', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(226, 'Ha Noi T T', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(227, 'Nam Dinh', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(228, 'Kalmar FF', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(229, 'Djurgårdens IF', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(230, 'JS Hercules', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(231, 'Närpes Kraft', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(232, 'FK Ventspils', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(233, 'FS Metta', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(234, 'Vendsyssel FF', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(235, 'OB', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(236, 'HJK', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(237, 'Pallokissat', '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(238, 'U. Católica de Quito', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(239, 'Barcelona SC', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(240, 'Ocean City Nor easters', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(241, 'New York Red Bulls U23', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(242, 'SIMA Aguilas', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(243, 'FC Miami City Champions', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(244, 'Santa Cruz Breakers FC', '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(245, 'San Francisco City FC', '2018-07-15 19:35:58', '2018-07-15 19:35:58');
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.migrations: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(6, '2016_06_01_000004_create_oauth_clients_table', 1),
	(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.oauth_access_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.oauth_auth_codes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.oauth_clients: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Laravel Personal Access Client', 'RRDZpoZnu0DPF4YEDE8L2jfhaL2g6iaZit0sMAwT', 'http://localhost', 1, 0, 0, '2018-07-15 08:35:20', '2018-07-15 08:35:20'),
	(2, NULL, 'Laravel Password Grant Client', 'kiEQPVX1LD98Lm2sz6J7AvNPdJAHo8rY15qlTvaI', 'http://localhost', 0, 1, 0, '2018-07-15 08:35:20', '2018-07-15 08:35:20');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.oauth_personal_access_clients: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2018-07-15 08:35:20', '2018-07-15 08:35:20');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.oauth_refresh_tokens: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.partido
CREATE TABLE IF NOT EXISTS `partido` (
  `idPartido` int(11) NOT NULL AUTO_INCREMENT,
  `idEquipoLocal` int(11) NOT NULL,
  `idEquipoVisita` int(11) NOT NULL,
  `minuto` int(11) DEFAULT NULL,
  `gl` int(11) DEFAULT NULL,
  `gv` int(11) DEFAULT NULL,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dia` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `a1` float DEFAULT NULL,
  `a2` float DEFAULT NULL,
  `ax` float DEFAULT NULL,
  `estado` enum('inactivo','jugando','finalizado') COLLATE utf8_unicode_ci DEFAULT 'jugando',
  PRIMARY KEY (`idPartido`),
  UNIQUE KEY `unico` (`dia`,`link`),
  KEY `fk_partido_equipo_idx` (`idEquipoLocal`),
  KEY `fk_partido_equipo1_idx` (`idEquipoVisita`),
  CONSTRAINT `fk_partido_equipo` FOREIGN KEY (`idEquipoLocal`) REFERENCES `equipo` (`idEquipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_partido_equipo1` FOREIGN KEY (`idEquipoVisita`) REFERENCES `equipo` (`idEquipo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.partido: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `partido` DISABLE KEYS */;
INSERT INTO `partido` (`idPartido`, `idEquipoLocal`, `idEquipoVisita`, `minuto`, `gl`, `gv`, `link`, `dia`, `created_at`, `updated_at`, `a1`, `a2`, `ax`, `estado`) VALUES
	(113, 222, 223, 45, 1, 1, '/pe/live/futbol/suecia/allsvenskan-suecia/ifk-norrkoping-bk-hacken', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 08:00:32', 2.45, 3.8, 2.55, 'finalizado'),
	(114, 224, 225, 45, 0, 0, '/pe/live/futbol/suecia/allsvenskan-suecia/gif-sundsvall-aik', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 08:00:32', 4.7, 2.45, 2.25, 'finalizado'),
	(115, 226, 227, 45, 0, 0, '/pe/live/futbol/asia/vietnam-v-league/ha-noi-t-t-nam-dinh', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 08:00:32', 1.46, 7, 3.55, 'finalizado'),
	(116, 228, 229, 45, 0, 1, '/pe/live/futbol/suecia/allsvenskan-suecia/kalmar-ff-djurgardens-if', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 08:00:32', 12.5, 1.32, 4.4, 'finalizado'),
	(117, 230, 231, 45, 1, 0, '/pe/live/futbol/finlandia/finlandia-2da-division-c/js-hercules-narpes-kraft', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 08:00:32', 1.11, 22, 7.6, 'finalizado'),
	(118, 232, 233, 45, 0, 1, '/pe/live/futbol/otras-ligas-europa/virsliga-letonia/fk-ventspils-fs-metta', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 07:56:21', 2.31, 2.96, 2.91, 'finalizado'),
	(119, 234, 235, 45, 0, 0, '/pe/live/futbol/dinamarca/superliga/vendsyssel-ff-ob', '2018-07-15', '2018-07-15 07:56:21', '2018-07-15 08:00:32', 5.9, 1.95, 2.6, 'finalizado'),
	(120, 236, 237, 45, 2, 0, '/pe/live/futbol/finlandia/naistenliiga-femenino/hjk-pallokissat', '2018-07-15', '2018-07-15 07:56:22', '2018-07-15 08:00:32', 1, 360, 70, 'finalizado'),
	(121, 238, 239, 92, 2, 3, '/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc', '2018-07-15', '2018-07-15 19:35:58', '2018-07-15 19:53:56', 270, 1.03, 16, 'finalizado'),
	(122, 240, 241, 93, 2, 1, '/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23', '2018-07-15', '2018-07-15 19:35:58', '2018-07-15 20:00:56', 1, 220, 58, 'finalizado'),
	(123, 242, 243, 88, 5, 0, '/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions', '2018-07-15', '2018-07-15 19:35:58', '2018-07-15 19:43:56', 1, 500, 500, 'jugando'),
	(124, 244, 245, 45, 0, 1, '/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc', '2018-07-15', '2018-07-15 19:35:58', '2018-07-15 20:03:56', 11, 1.26, 4.65, 'jugando');
/*!40000 ALTER TABLE `partido` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.partido_evento
CREATE TABLE IF NOT EXISTS `partido_evento` (
  `idPartidoEvento` int(11) NOT NULL AUTO_INCREMENT,
  `idPartido` int(11) NOT NULL,
  `minuto` int(11) DEFAULT NULL,
  `gl` int(11) DEFAULT NULL,
  `gv` int(11) DEFAULT NULL,
  `a1` float DEFAULT NULL,
  `ax` float DEFAULT NULL,
  `a2` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idPartidoEvento`),
  KEY `fk_partido_evento_partido1_idx` (`idPartido`),
  CONSTRAINT `fk_partido_evento_partido1` FOREIGN KEY (`idPartido`) REFERENCES `partido` (`idPartido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1933 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.partido_evento: ~31 rows (aproximadamente)
/*!40000 ALTER TABLE `partido_evento` DISABLE KEYS */;
INSERT INTO `partido_evento` (`idPartidoEvento`, `idPartido`, `minuto`, `gl`, `gv`, `a1`, `ax`, `a2`, `created_at`, `updated_at`) VALUES
	(1902, 113, 45, 1, 1, 2.45, 2.55, 3.8, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1903, 114, 45, 0, 0, 4.7, 2.25, 2.45, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1904, 115, 45, 0, 0, 1.46, 3.55, 7, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1905, 116, 45, 0, 1, 12.5, 4.4, 1.32, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1906, 117, 45, 1, 0, 1.11, 7.6, 22, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1907, 118, 45, 0, 1, 2.31, 2.91, 2.96, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1908, 119, 45, 0, 0, 5.9, 2.6, 1.95, '2018-07-15 07:56:21', '2018-07-15 07:56:21'),
	(1909, 120, 45, 2, 0, 1, 70, 360, '2018-07-15 07:56:22', '2018-07-15 07:56:22'),
	(1910, 116, 45, 0, 1, 12.5, 4.4, 1.32, '2018-07-15 07:58:09', '2018-07-15 07:58:09'),
	(1911, 117, 45, 1, 0, 1.11, 7.6, 22, '2018-07-15 07:58:09', '2018-07-15 07:58:09'),
	(1912, 118, 45, 0, 1, 2.31, 2.91, 2.96, '2018-07-15 07:58:09', '2018-07-15 07:58:09'),
	(1913, 120, 45, 2, 0, 1, 70, 360, '2018-07-15 07:58:09', '2018-07-15 07:58:09'),
	(1914, 116, 45, 0, 1, 12.5, 4.4, 1.32, '2018-07-15 07:58:33', '2018-07-15 07:58:33'),
	(1915, 117, 45, 1, 0, 1.11, 7.6, 22, '2018-07-15 07:58:33', '2018-07-15 07:58:33'),
	(1916, 118, 45, 0, 1, 2.31, 2.91, 2.96, '2018-07-15 07:58:33', '2018-07-15 07:58:33'),
	(1917, 120, 45, 2, 0, 1, 70, 360, '2018-07-15 07:58:33', '2018-07-15 07:58:33'),
	(1918, 116, 45, 0, 1, 12.5, 4.4, 1.32, '2018-07-15 07:59:37', '2018-07-15 07:59:37'),
	(1919, 117, 45, 1, 0, 1.11, 7.6, 22, '2018-07-15 07:59:37', '2018-07-15 07:59:37'),
	(1920, 118, 45, 0, 1, 2.31, 2.91, 2.96, '2018-07-15 07:59:37', '2018-07-15 07:59:37'),
	(1921, 120, 45, 2, 0, 1, 70, 360, '2018-07-15 07:59:37', '2018-07-15 07:59:37'),
	(1922, 116, 45, 0, 1, 12.5, 4.4, 1.32, '2018-07-15 08:00:32', '2018-07-15 08:00:32'),
	(1923, 117, 45, 1, 0, 1.11, 7.6, 22, '2018-07-15 08:00:32', '2018-07-15 08:00:32'),
	(1924, 118, 45, 0, 1, 2.31, 2.91, 2.96, '2018-07-15 08:00:32', '2018-07-15 08:00:32'),
	(1925, 120, 45, 2, 0, 1, 70, 360, '2018-07-15 08:00:32', '2018-07-15 08:00:32'),
	(1926, 121, 74, 1, 2, 17, 3.9, 1.29, '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(1927, 122, 70, 2, 1, 1.43, 3.45, 8.6, '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(1928, 123, 80, 4, 0, 1, 500, 500, '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(1929, 124, 31, 0, 1, 6.8, 4.2, 1.38, '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(1930, 123, 81, 5, 0, 1, 500, 500, '2018-07-15 19:36:56', '2018-07-15 19:36:56'),
	(1931, 121, 83, 1, 3, 220, 43, 1.01, '2018-07-15 19:44:56', '2018-07-15 19:44:56'),
	(1932, 121, 92, 2, 3, 270, 16, 1.03, '2018-07-15 19:52:56', '2018-07-15 19:52:56');
/*!40000 ALTER TABLE `partido_evento` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.partido_variable
CREATE TABLE IF NOT EXISTS `partido_variable` (
  `idPartidoVariable` int(11) NOT NULL AUTO_INCREMENT,
  `idPartido` int(11) NOT NULL,
  `minuto` int(11) NOT NULL,
  `a1` float DEFAULT NULL,
  `a2` float DEFAULT NULL,
  `ax` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gl` int(11) DEFAULT NULL,
  `gv` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPartidoVariable`),
  KEY `fk_partido_variable_partido1_idx` (`idPartido`),
  CONSTRAINT `fk_partido_variable_partido1` FOREIGN KEY (`idPartido`) REFERENCES `partido` (`idPartido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4821 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.partido_variable: ~123 rows (aproximadamente)
/*!40000 ALTER TABLE `partido_variable` DISABLE KEYS */;
INSERT INTO `partido_variable` (`idPartidoVariable`, `idPartido`, `minuto`, `a1`, `a2`, `ax`, `created_at`, `updated_at`, `gl`, `gv`) VALUES
	(4698, 113, 45, 2.45, 3.8, 2.55, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 1, 1),
	(4699, 114, 45, 4.7, 2.45, 2.25, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 0, 0),
	(4700, 115, 45, 1.46, 7, 3.55, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 0, 0),
	(4701, 116, 45, 12.5, 1.32, 4.4, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 0, 1),
	(4702, 117, 45, 1.11, 22, 7.6, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 1, 0),
	(4703, 118, 45, 2.31, 2.96, 2.91, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 0, 1),
	(4704, 119, 45, 5.9, 1.95, 2.6, '2018-07-15 07:56:21', '2018-07-15 07:56:21', 0, 0),
	(4705, 120, 45, 1, 360, 70, '2018-07-15 07:56:22', '2018-07-15 07:56:22', 2, 0),
	(4706, 113, 45, 2.45, 3.8, 2.55, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 1, 1),
	(4707, 114, 45, 4.7, 2.45, 2.25, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 0, 0),
	(4708, 115, 45, 1.46, 7, 3.55, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 0, 0),
	(4709, 116, 45, 12.5, 1.32, 4.4, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 0, 1),
	(4710, 117, 45, 1.11, 22, 7.6, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 1, 0),
	(4711, 118, 45, 2.31, 2.96, 2.91, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 0, 1),
	(4712, 119, 45, 5.9, 1.95, 2.6, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 0, 0),
	(4713, 120, 45, 1, 360, 70, '2018-07-15 07:58:09', '2018-07-15 07:58:09', 2, 0),
	(4714, 113, 45, 2.45, 3.8, 2.55, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 1, 1),
	(4715, 114, 45, 4.7, 2.45, 2.25, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 0, 0),
	(4716, 115, 45, 1.46, 7, 3.55, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 0, 0),
	(4717, 116, 45, 12.5, 1.32, 4.4, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 0, 1),
	(4718, 117, 45, 1.11, 22, 7.6, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 1, 0),
	(4719, 118, 45, 2.31, 2.96, 2.91, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 0, 1),
	(4720, 119, 45, 5.9, 1.95, 2.6, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 0, 0),
	(4721, 120, 45, 1, 360, 70, '2018-07-15 07:58:33', '2018-07-15 07:58:33', 2, 0),
	(4722, 113, 45, 2.45, 3.8, 2.55, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 1, 1),
	(4723, 114, 45, 4.7, 2.45, 2.25, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 0, 0),
	(4724, 115, 45, 1.46, 7, 3.55, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 0, 0),
	(4725, 116, 45, 12.5, 1.32, 4.4, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 0, 1),
	(4726, 117, 45, 1.11, 22, 7.6, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 1, 0),
	(4727, 118, 45, 2.31, 2.96, 2.91, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 0, 1),
	(4728, 119, 45, 5.9, 1.95, 2.6, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 0, 0),
	(4729, 120, 45, 1, 360, 70, '2018-07-15 07:59:37', '2018-07-15 07:59:37', 2, 0),
	(4730, 113, 45, 2.45, 3.8, 2.55, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 1, 1),
	(4731, 114, 45, 4.7, 2.45, 2.25, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 0, 0),
	(4732, 115, 45, 1.46, 7, 3.55, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 0, 0),
	(4733, 116, 45, 12.5, 1.32, 4.4, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 0, 1),
	(4734, 117, 45, 1.11, 22, 7.6, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 1, 0),
	(4735, 118, 45, 2.31, 2.96, 2.91, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 0, 1),
	(4736, 119, 45, 5.9, 1.95, 2.6, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 0, 0),
	(4737, 120, 45, 1, 360, 70, '2018-07-15 08:00:32', '2018-07-15 08:00:32', 2, 0),
	(4738, 121, 74, 17, 1.29, 3.9, '2018-07-15 19:35:58', '2018-07-15 19:35:58', 1, 2),
	(4739, 122, 70, 1.43, 8.6, 3.45, '2018-07-15 19:35:58', '2018-07-15 19:35:58', 2, 1),
	(4740, 123, 80, 1, 500, 500, '2018-07-15 19:35:58', '2018-07-15 19:35:58', 4, 0),
	(4741, 124, 31, 6.8, 1.38, 4.2, '2018-07-15 19:35:58', '2018-07-15 19:35:58', 0, 1),
	(4742, 121, 75, 17, 1.28, 3.95, '2018-07-15 19:36:56', '2018-07-15 19:36:56', 1, 2),
	(4743, 122, 71, 1.42, 9, 3.45, '2018-07-15 19:36:56', '2018-07-15 19:36:56', 2, 1),
	(4744, 123, 81, 1, 500, 500, '2018-07-15 19:36:56', '2018-07-15 19:36:56', 5, 0),
	(4745, 124, 32, 7, 1.39, 4.05, '2018-07-15 19:36:56', '2018-07-15 19:36:56', 0, 1),
	(4746, 121, 77, 21, 1.25, 4.15, '2018-07-15 19:37:56', '2018-07-15 19:37:56', 1, 2),
	(4747, 122, 73, 1.38, 10, 3.6, '2018-07-15 19:37:56', '2018-07-15 19:37:56', 2, 1),
	(4748, 123, 82, 1, 500, 500, '2018-07-15 19:37:56', '2018-07-15 19:37:56', 5, 0),
	(4749, 124, 33, 9.2, 1.29, 4.5, '2018-07-15 19:37:56', '2018-07-15 19:37:56', 0, 1),
	(4750, 121, 77, 22, 1.24, 4.25, '2018-07-15 19:38:56', '2018-07-15 19:38:56', 1, 2),
	(4751, 122, 74, 1.36, 11, 3.65, '2018-07-15 19:38:56', '2018-07-15 19:38:56', 2, 1),
	(4752, 123, 83, 1, 500, 500, '2018-07-15 19:38:56', '2018-07-15 19:38:56', 5, 0),
	(4753, 124, 34, 9.2, 1.3, 4.45, '2018-07-15 19:38:56', '2018-07-15 19:38:56', 0, 1),
	(4754, 121, 78, 25, 1.22, 4.4, '2018-07-15 19:39:56', '2018-07-15 19:39:56', 1, 2),
	(4755, 122, 74, 1.35, 12, 3.7, '2018-07-15 19:39:56', '2018-07-15 19:39:56', 2, 1),
	(4756, 123, 84, 1, 500, 500, '2018-07-15 19:39:56', '2018-07-15 19:39:56', 5, 0),
	(4757, 124, 35, 9.8, 1.28, 4.6, '2018-07-15 19:39:56', '2018-07-15 19:39:56', 0, 1),
	(4758, 121, 79, 29, 1.2, 4.6, '2018-07-15 19:40:56', '2018-07-15 19:40:56', 1, 2),
	(4759, 122, 75, 1.33, 13, 3.75, '2018-07-15 19:40:56', '2018-07-15 19:40:56', 2, 1),
	(4760, 123, 85, 1, 500, 500, '2018-07-15 19:40:56', '2018-07-15 19:40:56', 5, 0),
	(4761, 124, 36, 9.6, 1.28, 4.6, '2018-07-15 19:40:56', '2018-07-15 19:40:56', 0, 1),
	(4762, 121, 80, 32, 1.18, 4.8, '2018-07-15 19:41:56', '2018-07-15 19:41:56', 1, 2),
	(4763, 122, 77, 1.31, 14, 3.85, '2018-07-15 19:41:56', '2018-07-15 19:41:56', 2, 1),
	(4764, 123, 86, 1, 500, 500, '2018-07-15 19:41:56', '2018-07-15 19:41:56', 5, 0),
	(4765, 124, 37, 10, 1.27, 4.7, '2018-07-15 19:41:56', '2018-07-15 19:41:56', 0, 1),
	(4766, 121, 82, 38, 1.17, 5, '2018-07-15 19:42:56', '2018-07-15 19:42:56', 1, 2),
	(4767, 122, 77, 1.32, 13, 3.85, '2018-07-15 19:42:56', '2018-07-15 19:42:56', 2, 1),
	(4768, 123, 87, 1, 500, 500, '2018-07-15 19:42:56', '2018-07-15 19:42:56', 5, 0),
	(4769, 124, 38, 10, 1.27, 4.7, '2018-07-15 19:42:56', '2018-07-15 19:42:56', 0, 1),
	(4770, 121, 82, 200, 1.01, 31, '2018-07-15 19:43:56', '2018-07-15 19:43:56', 1, 2),
	(4771, 122, 78, 1.3, 15, 3.9, '2018-07-15 19:43:56', '2018-07-15 19:43:56', 2, 1),
	(4772, 123, 88, 1, 500, 500, '2018-07-15 19:43:56', '2018-07-15 19:43:56', 5, 0),
	(4773, 124, 39, 10, 1.27, 4.7, '2018-07-15 19:43:56', '2018-07-15 19:43:56', 0, 1),
	(4774, 121, 83, 220, 1.01, 43, '2018-07-15 19:44:56', '2018-07-15 19:44:56', 1, 3),
	(4775, 122, 79, 1.28, 16, 4.05, '2018-07-15 19:44:56', '2018-07-15 19:44:56', 2, 1),
	(4776, 124, 40, 11, 1.26, 4.65, '2018-07-15 19:44:56', '2018-07-15 19:44:56', 0, 1),
	(4777, 121, 85, 270, 1, 56, '2018-07-15 19:45:56', '2018-07-15 19:45:56', 1, 3),
	(4778, 122, 80, 1.32, 14, 3.75, '2018-07-15 19:45:56', '2018-07-15 19:45:56', 2, 1),
	(4779, 124, 41, 11, 1.26, 4.65, '2018-07-15 19:45:56', '2018-07-15 19:45:56', 0, 1),
	(4780, 121, 85, 260, 1, 60, '2018-07-15 19:46:56', '2018-07-15 19:46:56', 1, 3),
	(4781, 122, 82, 1.29, 16, 3.95, '2018-07-15 19:46:56', '2018-07-15 19:46:56', 2, 1),
	(4782, 124, 43, 12, 1.24, 4.8, '2018-07-15 19:46:56', '2018-07-15 19:46:56', 0, 1),
	(4783, 121, 86, 250, 1, 75, '2018-07-15 19:47:56', '2018-07-15 19:47:56', 1, 3),
	(4784, 122, 82, 1.27, 18, 4.05, '2018-07-15 19:47:56', '2018-07-15 19:47:56', 2, 1),
	(4785, 124, 43, 12, 1.24, 4.8, '2018-07-15 19:47:56', '2018-07-15 19:47:56', 0, 1),
	(4786, 121, 88, 260, 1, 100, '2018-07-15 19:48:56', '2018-07-15 19:48:56', 1, 3),
	(4787, 122, 83, 1.23, 23, 4.35, '2018-07-15 19:48:56', '2018-07-15 19:48:56', 2, 1),
	(4788, 124, 44, 12, 1.24, 4.85, '2018-07-15 19:48:56', '2018-07-15 19:48:56', 0, 1),
	(4789, 121, 89, 260, 1, 140, '2018-07-15 19:49:56', '2018-07-15 19:49:56', 1, 3),
	(4790, 122, 84, 1.21, 27, 4.5, '2018-07-15 19:49:56', '2018-07-15 19:49:56', 2, 1),
	(4791, 124, 45, 12, 1.24, 4.85, '2018-07-15 19:49:56', '2018-07-15 19:49:56', 0, 1),
	(4792, 121, 90, 260, 1, 130, '2018-07-15 19:50:56', '2018-07-15 19:50:56', 1, 3),
	(4793, 122, 86, 1.18, 33, 4.85, '2018-07-15 19:50:56', '2018-07-15 19:50:56', 2, 1),
	(4794, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:50:56', '2018-07-15 19:50:56', 0, 1),
	(4795, 121, 91, 260, 1, 170, '2018-07-15 19:51:56', '2018-07-15 19:51:56', 1, 3),
	(4796, 122, 86, 1.18, 34, 4.8, '2018-07-15 19:51:56', '2018-07-15 19:51:56', 2, 1),
	(4797, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:51:56', '2018-07-15 19:51:56', 0, 1),
	(4798, 121, 92, 270, 1.03, 16, '2018-07-15 19:52:56', '2018-07-15 19:52:56', 2, 3),
	(4799, 122, 87, 1.14, 45, 5.3, '2018-07-15 19:52:56', '2018-07-15 19:52:56', 2, 1),
	(4800, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:52:56', '2018-07-15 19:52:56', 0, 1),
	(4801, 121, 92, 270, 1.03, 16, '2018-07-15 19:53:56', '2018-07-15 19:53:56', 2, 3),
	(4802, 122, 89, 1.12, 60, 5.9, '2018-07-15 19:53:56', '2018-07-15 19:53:56', 2, 1),
	(4803, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:53:56', '2018-07-15 19:53:56', 0, 1),
	(4804, 122, 45, 1.08, 95, 7, '2018-07-15 19:54:56', '2018-07-15 19:54:56', 2, 1),
	(4805, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:54:56', '2018-07-15 19:54:56', 0, 1),
	(4806, 122, 91, 1.05, 170, 9.6, '2018-07-15 19:55:56', '2018-07-15 19:55:56', 2, 1),
	(4807, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:55:56', '2018-07-15 19:55:56', 0, 1),
	(4808, 122, 92, 1.02, 220, 17, '2018-07-15 19:56:56', '2018-07-15 19:56:56', 2, 1),
	(4809, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:56:56', '2018-07-15 19:56:56', 0, 1),
	(4810, 122, 92, 1, 220, 58, '2018-07-15 19:57:56', '2018-07-15 19:57:56', 2, 1),
	(4811, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:57:56', '2018-07-15 19:57:56', 0, 1),
	(4812, 122, 93, 1, 220, 58, '2018-07-15 19:58:56', '2018-07-15 19:58:56', 2, 1),
	(4813, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:58:56', '2018-07-15 19:58:56', 0, 1),
	(4814, 122, 93, 1, 220, 58, '2018-07-15 19:59:56', '2018-07-15 19:59:56', 2, 1),
	(4815, 124, 45, 13, 1.23, 4.85, '2018-07-15 19:59:56', '2018-07-15 19:59:56', 0, 1),
	(4816, 122, 93, 1, 220, 58, '2018-07-15 20:00:56', '2018-07-15 20:00:56', 2, 1),
	(4817, 124, 45, 13, 1.23, 4.85, '2018-07-15 20:00:56', '2018-07-15 20:00:56', 0, 1),
	(4818, 124, 45, 12, 1.25, 4.75, '2018-07-15 20:01:56', '2018-07-15 20:01:56', 0, 1),
	(4819, 124, 45, 12, 1.25, 4.75, '2018-07-15 20:02:56', '2018-07-15 20:02:56', 0, 1),
	(4820, 124, 45, 11, 1.26, 4.65, '2018-07-15 20:03:56', '2018-07-15 20:03:56', 0, 1);
/*!40000 ALTER TABLE `partido_variable` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.traking
CREATE TABLE IF NOT EXISTS `traking` (
  `idTraking` int(11) NOT NULL AUTO_INCREMENT,
  `data` longtext COLLATE utf8_unicode_ci,
  `procesado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idTraking`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.traking: ~34 rows (aproximadamente)
/*!40000 ALTER TABLE `traking` DISABLE KEYS */;
INSERT INTO `traking` (`idTraking`, `data`, `procesado`, `created_at`, `updated_at`) VALUES
	(149, '[{"equipo1":"IFK Norrköping","equipo2":"BK Häcken","link":"/pe/live/futbol/suecia/allsvenskan-suecia/ifk-norrkoping-bk-hacken","minuto":"","gl":"1","gv":"1","a1":"2.45","ax":"2.55","a2":"3.80"},{"equipo1":"GIF Sundsvall","equipo2":"AIK","link":"/pe/live/futbol/suecia/allsvenskan-suecia/gif-sundsvall-aik","minuto":"","gl":"0","gv":"0","a1":"4.70","ax":"2.25","a2":"2.45"},{"equipo1":"Ha Noi T T","equipo2":"Nam Dinh","link":"/pe/live/futbol/asia/vietnam-v-league/ha-noi-t-t-nam-dinh","minuto":"","gl":"0","gv":"0","a1":"1.46","ax":"3.55","a2":"7.00"},{"equipo1":"Kalmar FF","equipo2":"Djurgårdens IF","link":"/pe/live/futbol/suecia/allsvenskan-suecia/kalmar-ff-djurgardens-if","minuto":"","gl":"0","gv":"1","a1":"12.50","ax":"4.40","a2":"1.32"},{"equipo1":"JS Hercules","equipo2":"Närpes Kraft","link":"/pe/live/futbol/finlandia/finlandia-2da-division-c/js-hercules-narpes-kraft","minuto":"","gl":"1","gv":"0","a1":"1.11","ax":"7.60","a2":"22.00"},{"equipo1":"FK Ventspils","equipo2":"FS Metta","link":"/pe/live/futbol/otras-ligas-europa/virsliga-letonia/fk-ventspils-fs-metta","minuto":"","gl":"0","gv":"1","a1":"2.31","ax":"2.91","a2":"2.96"},{"equipo1":"Vendsyssel FF","equipo2":"OB","link":"/pe/live/futbol/dinamarca/superliga/vendsyssel-ff-ob","minuto":"","gl":"0","gv":"0","a1":"5.90","ax":"2.60","a2":"1.95"},{"equipo1":"HJK","equipo2":"Pallokissat","link":"/pe/live/futbol/finlandia/naistenliiga-femenino/hjk-pallokissat","minuto":"","gl":"2","gv":"0","a1":"-","ax":"70.00","a2":"360.00"}]', 1, '2018-07-15 07:56:22', '2018-07-15 07:56:22'),
	(150, '[{"equipo1":"IFK Norrköping","equipo2":"BK Häcken","link":"/pe/live/futbol/suecia/allsvenskan-suecia/ifk-norrkoping-bk-hacken","minuto":"","gl":"1","gv":"1","a1":"2.45","ax":"2.55","a2":"3.80"},{"equipo1":"GIF Sundsvall","equipo2":"AIK","link":"/pe/live/futbol/suecia/allsvenskan-suecia/gif-sundsvall-aik","minuto":"","gl":"0","gv":"0","a1":"4.70","ax":"2.25","a2":"2.45"},{"equipo1":"Ha Noi T T","equipo2":"Nam Dinh","link":"/pe/live/futbol/asia/vietnam-v-league/ha-noi-t-t-nam-dinh","minuto":"","gl":"0","gv":"0","a1":"1.46","ax":"3.55","a2":"7.00"},{"equipo1":"Kalmar FF","equipo2":"Djurgårdens IF","link":"/pe/live/futbol/suecia/allsvenskan-suecia/kalmar-ff-djurgardens-if","minuto":"","gl":"0","gv":"1","a1":"12.50","ax":"4.40","a2":"1.32"},{"equipo1":"JS Hercules","equipo2":"Närpes Kraft","link":"/pe/live/futbol/finlandia/finlandia-2da-division-c/js-hercules-narpes-kraft","minuto":"","gl":"1","gv":"0","a1":"1.11","ax":"7.60","a2":"22.00"},{"equipo1":"FK Ventspils","equipo2":"FS Metta","link":"/pe/live/futbol/otras-ligas-europa/virsliga-letonia/fk-ventspils-fs-metta","minuto":"","gl":"0","gv":"1","a1":"2.31","ax":"2.91","a2":"2.96"},{"equipo1":"Vendsyssel FF","equipo2":"OB","link":"/pe/live/futbol/dinamarca/superliga/vendsyssel-ff-ob","minuto":"","gl":"0","gv":"0","a1":"5.90","ax":"2.60","a2":"1.95"},{"equipo1":"HJK","equipo2":"Pallokissat","link":"/pe/live/futbol/finlandia/naistenliiga-femenino/hjk-pallokissat","minuto":"","gl":"2","gv":"0","a1":"-","ax":"70.00","a2":"360.00"}]', 1, '2018-07-15 07:58:09', '2018-07-15 07:58:09'),
	(151, '[{"equipo1":"IFK Norrköping","equipo2":"BK Häcken","link":"/pe/live/futbol/suecia/allsvenskan-suecia/ifk-norrkoping-bk-hacken","minuto":"","gl":"1","gv":"1","a1":"2.45","ax":"2.55","a2":"3.80"},{"equipo1":"GIF Sundsvall","equipo2":"AIK","link":"/pe/live/futbol/suecia/allsvenskan-suecia/gif-sundsvall-aik","minuto":"","gl":"0","gv":"0","a1":"4.70","ax":"2.25","a2":"2.45"},{"equipo1":"Ha Noi T T","equipo2":"Nam Dinh","link":"/pe/live/futbol/asia/vietnam-v-league/ha-noi-t-t-nam-dinh","minuto":"","gl":"0","gv":"0","a1":"1.46","ax":"3.55","a2":"7.00"},{"equipo1":"Kalmar FF","equipo2":"Djurgårdens IF","link":"/pe/live/futbol/suecia/allsvenskan-suecia/kalmar-ff-djurgardens-if","minuto":"","gl":"0","gv":"1","a1":"12.50","ax":"4.40","a2":"1.32"},{"equipo1":"JS Hercules","equipo2":"Närpes Kraft","link":"/pe/live/futbol/finlandia/finlandia-2da-division-c/js-hercules-narpes-kraft","minuto":"","gl":"1","gv":"0","a1":"1.11","ax":"7.60","a2":"22.00"},{"equipo1":"FK Ventspils","equipo2":"FS Metta","link":"/pe/live/futbol/otras-ligas-europa/virsliga-letonia/fk-ventspils-fs-metta","minuto":"","gl":"0","gv":"1","a1":"2.31","ax":"2.91","a2":"2.96"},{"equipo1":"Vendsyssel FF","equipo2":"OB","link":"/pe/live/futbol/dinamarca/superliga/vendsyssel-ff-ob","minuto":"","gl":"0","gv":"0","a1":"5.90","ax":"2.60","a2":"1.95"},{"equipo1":"HJK","equipo2":"Pallokissat","link":"/pe/live/futbol/finlandia/naistenliiga-femenino/hjk-pallokissat","minuto":"","gl":"2","gv":"0","a1":"-","ax":"70.00","a2":"360.00"}]', 1, '2018-07-15 07:58:33', '2018-07-15 07:58:33'),
	(152, '[{"equipo1":"IFK Norrköping","equipo2":"BK Häcken","link":"/pe/live/futbol/suecia/allsvenskan-suecia/ifk-norrkoping-bk-hacken","minuto":"","gl":"1","gv":"1","a1":"2.45","ax":"2.55","a2":"3.80"},{"equipo1":"GIF Sundsvall","equipo2":"AIK","link":"/pe/live/futbol/suecia/allsvenskan-suecia/gif-sundsvall-aik","minuto":"","gl":"0","gv":"0","a1":"4.70","ax":"2.25","a2":"2.45"},{"equipo1":"Ha Noi T T","equipo2":"Nam Dinh","link":"/pe/live/futbol/asia/vietnam-v-league/ha-noi-t-t-nam-dinh","minuto":"","gl":"0","gv":"0","a1":"1.46","ax":"3.55","a2":"7.00"},{"equipo1":"Kalmar FF","equipo2":"Djurgårdens IF","link":"/pe/live/futbol/suecia/allsvenskan-suecia/kalmar-ff-djurgardens-if","minuto":"","gl":"0","gv":"1","a1":"12.50","ax":"4.40","a2":"1.32"},{"equipo1":"JS Hercules","equipo2":"Närpes Kraft","link":"/pe/live/futbol/finlandia/finlandia-2da-division-c/js-hercules-narpes-kraft","minuto":"","gl":"1","gv":"0","a1":"1.11","ax":"7.60","a2":"22.00"},{"equipo1":"FK Ventspils","equipo2":"FS Metta","link":"/pe/live/futbol/otras-ligas-europa/virsliga-letonia/fk-ventspils-fs-metta","minuto":"","gl":"0","gv":"1","a1":"2.31","ax":"2.91","a2":"2.96"},{"equipo1":"Vendsyssel FF","equipo2":"OB","link":"/pe/live/futbol/dinamarca/superliga/vendsyssel-ff-ob","minuto":"","gl":"0","gv":"0","a1":"5.90","ax":"2.60","a2":"1.95"},{"equipo1":"HJK","equipo2":"Pallokissat","link":"/pe/live/futbol/finlandia/naistenliiga-femenino/hjk-pallokissat","minuto":"","gl":"2","gv":"0","a1":"-","ax":"70.00","a2":"360.00"}]', 1, '2018-07-15 07:59:37', '2018-07-15 07:59:37'),
	(153, '[{"equipo1":"IFK Norrköping","equipo2":"BK Häcken","link":"/pe/live/futbol/suecia/allsvenskan-suecia/ifk-norrkoping-bk-hacken","minuto":"","gl":"1","gv":"1","a1":"2.45","ax":"2.55","a2":"3.80"},{"equipo1":"GIF Sundsvall","equipo2":"AIK","link":"/pe/live/futbol/suecia/allsvenskan-suecia/gif-sundsvall-aik","minuto":"","gl":"0","gv":"0","a1":"4.70","ax":"2.25","a2":"2.45"},{"equipo1":"Ha Noi T T","equipo2":"Nam Dinh","link":"/pe/live/futbol/asia/vietnam-v-league/ha-noi-t-t-nam-dinh","minuto":"","gl":"0","gv":"0","a1":"1.46","ax":"3.55","a2":"7.00"},{"equipo1":"Kalmar FF","equipo2":"Djurgårdens IF","link":"/pe/live/futbol/suecia/allsvenskan-suecia/kalmar-ff-djurgardens-if","minuto":"","gl":"0","gv":"1","a1":"12.50","ax":"4.40","a2":"1.32"},{"equipo1":"JS Hercules","equipo2":"Närpes Kraft","link":"/pe/live/futbol/finlandia/finlandia-2da-division-c/js-hercules-narpes-kraft","minuto":"","gl":"1","gv":"0","a1":"1.11","ax":"7.60","a2":"22.00"},{"equipo1":"FK Ventspils","equipo2":"FS Metta","link":"/pe/live/futbol/otras-ligas-europa/virsliga-letonia/fk-ventspils-fs-metta","minuto":"","gl":"0","gv":"1","a1":"2.31","ax":"2.91","a2":"2.96"},{"equipo1":"Vendsyssel FF","equipo2":"OB","link":"/pe/live/futbol/dinamarca/superliga/vendsyssel-ff-ob","minuto":"","gl":"0","gv":"0","a1":"5.90","ax":"2.60","a2":"1.95"},{"equipo1":"HJK","equipo2":"Pallokissat","link":"/pe/live/futbol/finlandia/naistenliiga-femenino/hjk-pallokissat","minuto":"","gl":"2","gv":"0","a1":"-","ax":"70.00","a2":"360.00"}]', 1, '2018-07-15 08:00:32', '2018-07-15 08:00:32'),
	(154, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"74","gl":"1","gv":"2","a1":"17.00","ax":"3.90","a2":"1.29"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"70","gl":"2","gv":"1","a1":"1.43","ax":"3.45","a2":"8.60"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"80","gl":"4","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"31","gl":"0","gv":"1","a1":"6.80","ax":"4.20","a2":"1.38"}]', 1, '2018-07-15 19:35:58', '2018-07-15 19:35:58'),
	(155, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"75","gl":"1","gv":"2","a1":"17.00","ax":"3.95","a2":"1.28"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"71","gl":"2","gv":"1","a1":"1.42","ax":"3.45","a2":"9.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"81","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"32","gl":"0","gv":"1","a1":"7.00","ax":"4.05","a2":"1.39"}]', 1, '2018-07-15 19:36:56', '2018-07-15 19:36:56'),
	(156, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"77","gl":"1","gv":"2","a1":"21.00","ax":"4.15","a2":"1.25"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"73","gl":"2","gv":"1","a1":"1.38","ax":"3.60","a2":"10.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"82","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"33","gl":"0","gv":"1","a1":"9.20","ax":"4.50","a2":"1.29"}]', 1, '2018-07-15 19:37:56', '2018-07-15 19:37:56'),
	(157, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"77","gl":"1","gv":"2","a1":"22.00","ax":"4.25","a2":"1.24"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"74","gl":"2","gv":"1","a1":"1.36","ax":"3.65","a2":"11.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"83","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"34","gl":"0","gv":"1","a1":"9.20","ax":"4.45","a2":"1.30"}]', 1, '2018-07-15 19:38:56', '2018-07-15 19:38:56'),
	(158, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"78","gl":"1","gv":"2","a1":"25.00","ax":"4.40","a2":"1.22"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"74","gl":"2","gv":"1","a1":"1.35","ax":"3.70","a2":"12.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"84","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"35","gl":"0","gv":"1","a1":"9.80","ax":"4.60","a2":"1.28"}]', 1, '2018-07-15 19:39:56', '2018-07-15 19:39:56'),
	(159, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"79","gl":"1","gv":"2","a1":"29.00","ax":"4.60","a2":"1.20"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"75","gl":"2","gv":"1","a1":"1.33","ax":"3.75","a2":"13.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"85","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"36","gl":"0","gv":"1","a1":"9.60","ax":"4.60","a2":"1.28"}]', 1, '2018-07-15 19:40:56', '2018-07-15 19:40:56'),
	(160, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"80","gl":"1","gv":"2","a1":"32.00","ax":"4.80","a2":"1.18"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"77","gl":"2","gv":"1","a1":"1.31","ax":"3.85","a2":"14.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"86","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"37","gl":"0","gv":"1","a1":"10.00","ax":"4.70","a2":"1.27"}]', 1, '2018-07-15 19:41:56', '2018-07-15 19:41:56'),
	(161, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"82","gl":"1","gv":"2","a1":"38.00","ax":"5.00","a2":"1.17"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"77","gl":"2","gv":"1","a1":"1.32","ax":"3.85","a2":"13.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"87","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"38","gl":"0","gv":"1","a1":"10.00","ax":"4.70","a2":"1.27"}]', 1, '2018-07-15 19:42:56', '2018-07-15 19:42:56'),
	(162, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"82","gl":"1","gv":"2","a1":"200.00","ax":"31.00","a2":"1.01"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"78","gl":"2","gv":"1","a1":"1.30","ax":"3.90","a2":"15.00"},{"equipo1":"SIMA Aguilas","equipo2":"FC Miami City Champions","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/sima-aguilas-fc-miami-city-champions","minuto":"88","gl":"5","gv":"0","a1":"-","ax":"500.00","a2":"500.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"39","gl":"0","gv":"1","a1":"10.00","ax":"4.70","a2":"1.27"}]', 1, '2018-07-15 19:43:56', '2018-07-15 19:43:56'),
	(163, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"83","gl":"1","gv":"3","a1":"220.00","ax":"43.00","a2":"1.01"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"79","gl":"2","gv":"1","a1":"1.28","ax":"4.05","a2":"16.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"40","gl":"0","gv":"1","a1":"11.00","ax":"4.65","a2":"1.26"}]', 1, '2018-07-15 19:44:56', '2018-07-15 19:44:56'),
	(164, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"85","gl":"1","gv":"3","a1":"270.00","ax":"56.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"80","gl":"2","gv":"1","a1":"1.32","ax":"3.75","a2":"14.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"41","gl":"0","gv":"1","a1":"11.00","ax":"4.65","a2":"1.26"}]', 1, '2018-07-15 19:45:56', '2018-07-15 19:45:56'),
	(165, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"85","gl":"1","gv":"3","a1":"260.00","ax":"60.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"82","gl":"2","gv":"1","a1":"1.29","ax":"3.95","a2":"16.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"43","gl":"0","gv":"1","a1":"12.00","ax":"4.80","a2":"1.24"}]', 1, '2018-07-15 19:46:56', '2018-07-15 19:46:56'),
	(166, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"86","gl":"1","gv":"3","a1":"250.00","ax":"75.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"82","gl":"2","gv":"1","a1":"1.27","ax":"4.05","a2":"18.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"43","gl":"0","gv":"1","a1":"12.00","ax":"4.80","a2":"1.24"}]', 1, '2018-07-15 19:47:56', '2018-07-15 19:47:56'),
	(167, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"88","gl":"1","gv":"3","a1":"260.00","ax":"100.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"83","gl":"2","gv":"1","a1":"1.23","ax":"4.35","a2":"23.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"44","gl":"0","gv":"1","a1":"12.00","ax":"4.85","a2":"1.24"}]', 1, '2018-07-15 19:48:56', '2018-07-15 19:48:56'),
	(168, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"89","gl":"1","gv":"3","a1":"260.00","ax":"140.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"84","gl":"2","gv":"1","a1":"1.21","ax":"4.50","a2":"27.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"45","gl":"0","gv":"1","a1":"12.00","ax":"4.85","a2":"1.24"}]', 1, '2018-07-15 19:49:56', '2018-07-15 19:49:56'),
	(169, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"90","gl":"1","gv":"3","a1":"260.00","ax":"130.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"86","gl":"2","gv":"1","a1":"1.18","ax":"4.85","a2":"33.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:50:56', '2018-07-15 19:50:56'),
	(170, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"91","gl":"1","gv":"3","a1":"260.00","ax":"170.00","a2":"-"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"86","gl":"2","gv":"1","a1":"1.18","ax":"4.80","a2":"34.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:51:56', '2018-07-15 19:51:56'),
	(171, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"92","gl":"2","gv":"3","a1":"270.00","ax":"16.00","a2":"1.03"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"87","gl":"2","gv":"1","a1":"1.14","ax":"5.30","a2":"45.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:52:56', '2018-07-15 19:52:56'),
	(172, '[{"equipo1":"U. Católica de Quito","equipo2":"Barcelona SC","link":"/pe/live/futbol/otras-ligas-sudamerica/ecuador-primera-a/u-catolica-de-quito-barcelona-sc","minuto":"92","gl":"2","gv":"3","a1":"270.00","ax":"16.00","a2":"1.03"},{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"89","gl":"2","gv":"1","a1":"1.12","ax":"5.90","a2":"60.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:53:56', '2018-07-15 19:53:56'),
	(173, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"","gl":"2","gv":"1","a1":"1.08","ax":"7.00","a2":"95.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:54:56', '2018-07-15 19:54:56'),
	(174, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"91","gl":"2","gv":"1","a1":"1.05","ax":"9.60","a2":"170.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:55:56', '2018-07-15 19:55:56'),
	(175, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"92","gl":"2","gv":"1","a1":"1.02","ax":"17.00","a2":"220.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:56:56', '2018-07-15 19:56:56'),
	(176, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"92","gl":"2","gv":"1","a1":"-","ax":"58.00","a2":"220.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:57:56', '2018-07-15 19:57:56'),
	(177, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"93","gl":"2","gv":"1","a1":"-","ax":"58.00","a2":"220.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:58:56', '2018-07-15 19:58:56'),
	(178, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"93","gl":"2","gv":"1","a1":"-","ax":"58.00","a2":"220.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 19:59:56', '2018-07-15 19:59:56'),
	(179, '[{"equipo1":"Ocean City Nor easters","equipo2":"New York Red Bulls U23","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/ocean-city-nor-easters-new-york-red-bulls-u23","minuto":"93","gl":"2","gv":"1","a1":"-","ax":"58.00","a2":"220.00"},{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"13.00","ax":"4.85","a2":"1.23"}]', 1, '2018-07-15 20:00:56', '2018-07-15 20:00:56'),
	(180, '[{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"12.00","ax":"4.75","a2":"1.25"}]', 1, '2018-07-15 20:01:56', '2018-07-15 20:01:56'),
	(181, '[{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"12.00","ax":"4.75","a2":"1.25"}]', 1, '2018-07-15 20:02:56', '2018-07-15 20:02:56'),
	(182, '[{"equipo1":"Santa Cruz Breakers FC","equipo2":"San Francisco City FC","link":"/pe/live/futbol/centro-y-norteamerica/usa-football/santa-cruz-breakers-fc-san-francisco-city-fc","minuto":"","gl":"0","gv":"1","a1":"11.00","ax":"4.65","a2":"1.26"}]', 1, '2018-07-15 20:03:56', '2018-07-15 20:03:56');
/*!40000 ALTER TABLE `traking` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla db_betsson.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Guillermo', 'gdavidas3@gmail.com', '$2y$10$ENu/SbAf77H5xDJrEtL12.ujRDoak02.q/t.t9Xco5RonXR6kph/2', NULL, '2018-07-15 08:54:49', '2018-07-15 08:54:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `google` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `correo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` enum('activo','inactivo') COLLATE utf8_unicode_ci DEFAULT 'inactivo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `Índice 2` (`google`),
  UNIQUE KEY `Índice 3` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.usuario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`idUsuario`, `google`, `correo`, `nombre`, `foto`, `estado`, `created_at`, `updated_at`) VALUES
	(1, '100061747592398441364', 'gdavidas3@gmail.com', 'Guillermo David Asto', 'https://lh6.googleusercontent.com/-sEycCcUJvv0/AAAAAAAAAAI/AAAAAAAAHyM/XHlY1EVcNDM/s96-c/photo.jpg', 'inactivo', '2018-07-17 20:41:58', '2018-07-17 20:41:58');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Volcando estructura para tabla db_betsson.usuario_sesion
CREATE TABLE IF NOT EXISTS `usuario_sesion` (
  `idUsuarioSesion` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `token` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaFin` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idUsuarioSesion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla db_betsson.usuario_sesion: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_sesion` DISABLE KEYS */;
INSERT INTO `usuario_sesion` (`idUsuarioSesion`, `idUsuario`, `token`, `fechaFin`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 'fc506c74ed58a09e26231f5e5e8fb254', '2018-07-18 00:00:00', 1, '2018-07-17 20:53:34', '2018-07-17 20:53:34'),
	(2, 1, '10d1bf22c0a376b51f7cbbf7a4c68b5f', '2018-07-24 21:01:17', 1, '2018-07-17 21:01:17', '2018-07-17 21:01:17'),
	(3, 1, '19d2c9e297971b5e1cc73572817c07cc', '2018-07-24 21:07:12', 1, '2018-07-17 21:07:12', '2018-07-17 21:07:12'),
	(4, 1, '0f793055c7dff280bde24c7ba07c741a', '2018-07-24 21:19:49', 1, '2018-07-17 21:19:49', '2018-07-17 21:19:49'),
	(5, 1, 'c5eb723f01d15d8e8308899cdc1c348b', '2018-07-24 21:22:55', 1, '2018-07-17 21:22:55', '2018-07-17 21:22:55');
/*!40000 ALTER TABLE `usuario_sesion` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
