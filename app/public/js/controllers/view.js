app.controller("viewsController", function ($scope, $rootScope, $timeout,$http) {
// app.controller("viewsController", function($scope,$rootScope) {

    $rootScope.pageCurrent = $rootScope.page;
    $rootScope.usuario = false;
    $rootScope.changePage = function (vista, data) {
        $rootScope.page = vista;
        $rootScope.pageCurrent = vista;
        $rootScope.$broadcast($scope.page, data);
        try {
            $scope.$digest();
        } catch (e) {
            //console.log("error",e);
        }
    }
    $("#apuestas").fadeIn();
    //console.log("eeee",$rootScope.isLogin() )
    var intentos = 0;
    if( $rootScope.isLogin() ){
        validarSesion();
    }
    else{
        irLogin();
    }
    function validarSesion(){
        if(intentos >= 3){
            irLogin();
            return;
        }
        var url = config.api+"/sesion/"+localStorage.getItem("token");
        $http.get(url).then(function(res){
            if(res.data.status == 1){
                $rootScope.usuario = res.data.usuario;
                $rootScope.changePage("home");
            }
            else{
                setTimeout(validarSesion,1000);
            }
        },function(error){
            setTimeout(validarSesion,1000);
        });
        intentos++;
    }
    function irLogin(){
        localStorage.clear();
        $rootScope.changePage("login");
    }
    $rootScope.showModal = function (name) {
        //console.log("abrir modal", name);
        $rootScope.$broadcast(name);
    }
    function cambioRuta(){
        
    }
    function init(){
        $(window).on("hashchange",cambioRuta);
    }
    init();
});