var app = angular.module("apuestas", []);
app.run(function($rootScope) {
	$rootScope.getForm = function(array){
		var formData = new FormData();
		var i,data,ar;
		var token = localStorage.getItem("token");
		formData.append("token",token);
		for ( i = 0; i < array.length; i++) {
			ar = array[i];
			formData.append(ar[0],ar[1]);
		}
		return formData;
	}
	$rootScope.isLogin = function(){
		var token = localStorage.getItem("token");
		if(token){
			return true;
		}
		return false;
	}
});

app.directive('customOnChange', function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          var onChangeFunc = scope.$eval(attrs.customOnChange);
          element.bind('change', onChangeFunc);
        }
    };
});
var configPost = {
    headers : {
        'Content-Type': 'application/json;charset=utf-8;'
    }
};
var configFile = {
    headers : {
        'Content-Type': 'multipart/form-data; boundary=something;'
    }
};