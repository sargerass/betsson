<div class="view" id="homeView" ng-controller="homeController" ng-show="pageCurrent =='@{{page.name}}'">
	<h1 class="text-center">
		En vivo
	</h1>
	<div class="row">
		<div class="col-md-4 col-sm-6" ng-repeat="partido in arrayPartidos" >
			<div class="card text-center">
				<div class="card-header">
					@{{partido.local}} VS
					@{{partido.visita}}
				</div>
				<div class="card-body">
					<h5 class="card-title">
						@{{partido.gl}} -
						@{{partido.gv}}
					</h5>
					<p class="card-text">
						Minuto: @{{partido.minuto}}
					</p>
					<h5 class="card-title">
						@{{partido.a1}} - @{{partido.ax}} - @{{partido.a2}}
					</h5>					
				</div>
				<div class="card-footer text-muted">
					
					<a href="#" class="btn btn-primary">Apostar</a>
				</div>
			</div>
		</div>
	</div>
</div>