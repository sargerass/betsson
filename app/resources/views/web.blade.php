<!DOCTYPE html>
<html lang="es">
<head>
    <title>El midas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/fuentes.css">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{Config::get('app.url')}}/css/general.css">

    @foreach($arrayComponents as $key => $file)
        <link rel='stylesheet' type='text/css' href='css/sections/{{$file}}.css'>      
    @endforeach
    @foreach($arrayModals as $key => $file)
        <link rel='stylesheet' type='text/css' href='css/modals/{{$file}}.css'>     
    @endforeach
    <meta charset="utf-8">
    <meta name="google-signin-client_id" content="{{Config::get('app.gl_id')}}.apps.googleusercontent.com">
    <script type="text/javascript">
    var config ={
        api: "{{Config::get('app.url')}}/api",
        web:"{{Config::get('app.url')}}"
    }
    </script>
    <script src="https://apis.google.com/js/platform.js?onload=validarLoginGL" async defer></script>
</head>
<body>
    <div id="apuestas" class="" ng-app="apuestas" ng-controller="viewsController">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" ng-show="usuario">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#">
                    <img src="{{Config::get('app.url')}}/img/icono.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Midas
                </a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#bets">Apuestas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#historial">Historial</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">

                    <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      @{{usuario.nombre}}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                      <a class="dropdown-item" href="#">Salir</a>
                      <a class="dropdown-item" href="#">Betsson</a>
                    </div>
                  </div>
                </form>
            </div>
        </nav>
        <div class="container">
            @foreach($arrayComponents as $key => $file)
                @include("sections.$file")     
            @endforeach
            @foreach($arrayModals as $key => $file)
                @include("modals.$file")
            @endforeach    
        </div>
        
        <div id="loader">
            
        </div>
    </div>
    @if( Config("app.debug") )
        @foreach($arrayScripts as $key => $file)
            <script type='text/javascript' src='{{$file}}'></script>
        @endforeach
        @foreach($arrayComponents as $key => $file)
            <script type='text/javascript' src='js/controllers/sections/{{$file}}.js'></script>
        @endforeach
        @foreach($arrayModals as $key => $file)
            <script type='text/javascript' src='js/controllers/modals/{{$file}}.js'></script>
        @endforeach
    @else

    @endif

</body>
</html>
