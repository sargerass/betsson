<!DOCTYPE html>
<html>
<head>
	<title>Noti</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
		table{
			width: 100%;
		}
	</style>
</head>
<body>
	<div >
		@if(  COUNT($arrayGoleadas) > 0)
		<h1>
			Goleadas
		</h1>
		<table border="2">
			<thead>
				<tr>
					<th>
						Minuto
					</th>
					<th>
						Score
					</th>
					<th>
						A1
					</th>
					<th>
						A2
					</th>
					<th>
						Link
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($arrayGoleadas as $key => $partido )
				<tr>
					<td>
						{{$partido->minuto}}
					</td>
					<td>
						{{$partido->gl}} - {{$partido->gv}}
					</td>
					<td>
						{{$partido->a1}}
					</td>
					<td>
						{{$partido->a2}}
					</td>
					<td>
						<a href="{{Config::get('app.betsson')}}{{$partido->link}}" target="_blank">
							Go!!!
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
		@if(  COUNT($arrayPocoRiesgo) > 0)
		<h1>
			Bajo riesgo
		</h1>
		<table border="2">
			<thead>
				<tr>
					<th>
						Minuto
					</th>
					<th>
						Score
					</th>
					<th>
						A1
					</th>
					<th>
						A2
					</th>
					<th>
						Link
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($arrayPocoRiesgo as $key => $partido )
				<tr>
					<td>
						{{$partido->minuto}}
					</td>
					<td>
						{{$partido->gl}} - {{$partido->gv}}
					</td>
					<td>
						{{$partido->a1}}
					</td>
					<td>
						{{$partido->a2}}
					</td>
					<td>
						<a href="{{Config::get('app.betsson')}}{{$partido->link}}" target="_blank">
							Go!!!
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
	</div>
</body>
</html>