<?php
    require_once("config.php");
    $codigoRecuperacion = "";
    if(isset($_GET["codigo"])){
    $codigoRecuperacion = $_GET["codigo"];
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Bebé Aventurero II</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/fuentes.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="css/general.css">

    @foreach($arrayComponent as $key => $component)


    @endforeach
    <?php
        $arrayComponents = [
            "login","inicio","dash","detalle","apuestas"
        ];
        $arrayModals = [
            //"modalCodigos","modalCompartir","modalSelecFecha","modalTerminos","modalLugares"
        ];
        foreach ($arrayComponents as $key => $value) {
            $link = $value;
            echo "<link rel='stylesheet' type='text/css' href='css/sections/$link.css'>";
        }
        foreach ($arrayModals as $key => $value) {
            $link = $value;
            echo "<link rel='stylesheet' type='text/css' href='css/modals/$link.css'>";
        }
    ?>
    <meta charset="utf-8">
    <script type="text/javascript">
    var config ={
        api: "Config::get('url')/api",
        web:"Config::get('url')"
    }
    </script>

</head>
<body>
    <div id="apuestas" class="" ng-app="apuestas" ng-controller="viewsController">
        <?php
            foreach ($arrayComponents as $key => $value) {
                $link = $value;

                include("sections/$link.php");
            }
            foreach ($arrayModals as $key => $value) {
                $link = $value;
                include("modals/$link.php");
            }
         ?>
        <div id="loader">
            
        </div>
    </div>
    
    <?php
        $arrayScripts = [
            "js/vendor/jquery.min.js",
            "js/vendor/angular.min.js",
            "js/app.js",
            "js/controllers/views.js",
        ];
        foreach ($arrayScripts as $key => $value) {
            $link = $value;
            echo "<script type='text/javascript' src='$link'></script>";
        }
        foreach ($arrayComponents as $key => $value) {
            $link = $value;
            echo "<script type='text/javascript' src='js/controllers/sections/$link.js'></script>";
        }
        foreach ($arrayModals as $key => $value) {
            $link = $value;
            echo "<script type='text/javascript' src='js/controllers/modals/$link.js'></script>";
        }
     ?>

</body>
</html>
