<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    public $table = "equipo";
    public $primaryKey = "idEquipo";
    public static function nombre($nombre){
    	$equipo = Equipo::where("nombre",$nombre)->first();
        if($equipo == null){
            $equipo = new Equipo();
            $equipo->nombre = $nombre;
            $equipo->save();
        }
        return $equipo;
    }
}
