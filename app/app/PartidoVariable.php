<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidoVariable extends Model
{
    public $table = "partido_variable";
    public $primaryKey = "idPartidoVariable";
}
