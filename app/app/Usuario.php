<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "usuario";
   	protected $primaryKey = "idUsuario";
   	protected $fillable = [
		'google',
		'correo',
		'nombre',
		'foto'
	];
	public static function notificables(){
		return Usuario::where("notificacion",1)
				->where("estado","activo")
				->get();
	}
}
