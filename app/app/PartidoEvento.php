<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartidoEvento extends Model
{
    public $table = "partido_evento";
    public $primaryKey = "idPartidoEvento";
}
