<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helps\UUID;
class UsuarioSesion extends Model
{
    protected $table = "usuario_sesion";
   	protected $primaryKey = "idUsuarioSesion";
   	protected $fillable = [
		'idUsuario',
		'token',
		'fechaFin'
	];
	public static function crearSesion($idUsuario){
		$fecha = new \DateTime();
		$fecha->modify("+7 DAY");
		$fechaFin = $fecha->format("Y-m-d H:i:s");
		$token = UUID::v1();
		$datos = [
			"idUsuario"=>$idUsuario,
			"token"=>$token,
			"fechaFin"=>$fechaFin
		];
		return UsuarioSesion::create($datos);
	}
}
