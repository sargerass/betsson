<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    public $table = "partido";
    public $primaryKey = "idPartido";
}
