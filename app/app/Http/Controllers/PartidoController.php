<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;
use App\Partido;
use App\PartidoEvento;
use App\PartidoVariable;
use App\Traking;
use DB;
class PartidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arrayPartidos = DB::table("partido as A")
            ->join("equipo as B","A.idEquipoLocal","=","B.idEquipo")
            ->join("equipo as C","A.idEquipoVisita","=","C.idEquipo")
            ->where("A.estado","jugando")
            ->select("A.idPartido","B.nombre as local","C.nombre as visita",
                "A.minuto","A.gl","A.gv","A.link","A.dia","A.a1","A.a2","A.ax")
            ->get();
        return $arrayPartidos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idPartido)
    {
        $partido = Partido::find($idPartido);
        $arrayEventos = PartidoEvento::where("idPartido",$idPartido)->get();
        return ["arrayEventos",$arrayEventos];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
