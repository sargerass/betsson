<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;
use App\Partido;
use App\PartidoEvento;
use App\PartidoVariable;
use App\Traking;
use App\Usuario;
use DB;
use Config;
use Mail;
class RobotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $request = new \stdClass();
        $request->equipo1 = "Alianza";
        $request->equipo2 = "Uniersitario";
        $request->link = "fb.com";
        $request->gl = 1;
        $request->gv = 0;
        $request->minuto = 20;
        $request->a1 = 1.5;
        $request->ax = 3.2;
        $request->a2 = 7.5;
        $this->guardarPartido($request);
    }
    private function validateVar($txt){
        if( !is_numeric($txt) ){
            return 1;
        }
        return $txt;
    }
    private function validateGol($txt){
        if( !is_numeric($txt) ){
            return 0;
        }
        return $txt;
    }
    private function guardarPartido($request){
        $date = new \DateTime();
        $dia = $date->format("Y-m-d");
        $n1 = $request->equipo1;
        $n2 = $request->equipo2;
        $link = $request->link;
        $request->a1 = $this->validateVar($request->a1);
        $request->ax = $this->validateVar($request->ax);
        $request->a2 = $this->validateVar($request->a2);
        $request->gl = $this->validateGol($request->gl);
        $request->gv = $this->validateGol($request->gv);
        $equipo1 = Equipo::nombre($n1);
        $equipo2 = Equipo::nombre($n2);
        $partido = Partido::where("link",$link)->where("dia",$dia)->first();
        if( !is_numeric($request->minuto) ){
            $request->minuto = 45;
        }
        if($partido == null){
            $partido = new Partido();
            $partido->idEquipoLocal = $equipo1->idEquipo;
            $partido->idEquipoVisita = $equipo2->idEquipo;            
            $partido->link = $request->link;
            $partido->dia = $dia;            
        }
        $partido->gv = $request->gv;
        $partido->gl = $request->gl;
        $partido->minuto = $request->minuto;
        $partido->a1 = $request->a1;
        $partido->ax = $request->ax;
        $partido->a2 = $request->a2;
        $partido->save();

        $idPartido = $partido->idPartido;
        $pv = new PartidoVariable();
        $pv->idPartido = $idPartido;
        $pv->a1 = $request->a1;
        $pv->ax = $request->ax;
        $pv->a2 = $request->a2;
        $pv->gl = $request->gl;
        $pv->gv = $request->gv;
        $pv->minuto = $request->minuto;
        $pv->save();
        $evento = PartidoEvento::where("idPartido",$idPartido)
        ->where("gl",$request->gl)->where("gv",$request->gv)->first();
        if($evento == null){
            $evento = new PartidoEvento();
            $evento->idPartido = $idPartido;
            $evento->minuto = $request->minuto;
            $evento->gl = $request->gl;
            $evento->gv = $request->gv;
            $evento->a1 = $request->a1;
            $evento->ax = $request->ax;
            $evento->a2 = $request->a2;
            $evento->save();
        }
        return $partido;
    }
    private function validarEvento(){
        $sql = "SELECT idPartido,abs(gl-gv)
                FROM partido
                WHERE estado = 'jugando' AND minuto > 70 AND abs(gl-gv) >= 2 
                GROUP BY idPartido";
        $arrayRes = DB::select($sql);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $traking = new Traking();
        $traking->data = $request->data;
        $array = json_decode( $request->data);
        foreach ($array as $key => $value) {
            $this->guardarPartido($value);
        }
        $traking->procesado = 1;
        $traking->save();
        $sql = "UPDATE partido SET estado = 'finalizado'
                WHERE estado = 'jugando' AND  
                ( minuto > 90 OR  TIMESTAMPDIFF(HOUR,created_at,now()) > 3 )";

        DB::update($sql);
        $this->validarNotificaciones();
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }
    private function validarNotificaciones(){
        $arrayGoleadas = $this->goleadas();
        $arrayPocoRiesgo = $this->pocoRiesgo();
        if( COUNT($arrayGoleadas) == 0 && COUNT($arrayPocoRiesgo) == 0 ){
            return 1;
        }
        $datos = ["arrayGoleadas"=>$arrayGoleadas,"arrayPocoRiesgo"=>$arrayPocoRiesgo];
        /*
        $arrayUsuarios = Usuario::notificables();
        
        foreach ($arrayUsuarios as $key => $value) {
            $this->enviarCorreo($value->correo,$value->nombre,$datos);
        }
        */
        $arrayActualizacion = [];
        $arrayPartidos = array_merge($arrayGoleadas,$arrayPocoRiesgo);
        foreach ($arrayPartidos as $key => $value) {
            $arrayActualizacion[] = $value->idPartido;
        }
        Partido::whereIn("idPartido",$arrayActualizacion)
            ->update(["notificado"=>1]);
        return ["status"=>1,"mensaje"=>"Notificados"];
    }
    private function goleadas(){
        $url = Config::get("app.url_betsson");
        $sql = "SELECT idPartido,minuto,gl,gv,a1,a2,link
                FROM partido 
                WHERE
                estado='jugando' AND notificado IS NULL  
                AND ABS(gl - gv) >= 3 AND a1>1 AND a2>1
                AND estado='jugando' 
                ";
        $arrayPartidos = DB::select($sql);
        return $arrayPartidos;
    }
    private function pocoRiesgo(){
        $url = Config::get("app.url_betsson");
        $sql = "SELECT idPartido,minuto,gl,gv,a1,a2,link
                FROM partido 
                WHERE
                estado='jugando' AND notificado IS NULL  
                AND ABS(gl - gv) = 2 AND a1>1.01 AND a2>1.01
                AND minuto > 70
                ";
        $arrayPartidos = DB::select($sql);
        return $arrayPartidos;
    }
    private function enviarCorreo($correo,$nombre,$datos){

        $usuario = new \stdClass();
        // return json_encode($usuario);
        $usuario->correo = $correo;
        $usuario->nombre = $nombre;
        //return view("correos.notificacion",$datos);
        Mail::send('correos.notificacion', $datos ,
            function($message) use ($usuario)
            {
                $message->to($usuario->correo,$usuario->nombre)
                        ->subject('Cargo tu midas');
            }
        );
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
