/* Listen for messages */
chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
  	if ( msg.command && ( msg.command == "obtener_informe" ) ) {
		let informe = enviar_info()
		sendResponse(informe);
  	}
});

let enviar_info = () => {
	return $(".informe_json").text();
}