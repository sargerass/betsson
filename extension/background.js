
let injectar_script = function(tab){
	chrome.tabs.executeScript(tab.id, {
		code: 'console.log("injectando script");'
	}, function(result){
		chrome.tabs.executeScript( tab.id, { file: 'jquery-3.2.1.min.js' });
		chrome.tabs.executeScript( tab.id, { file: 'script.js' });
	});
	//inicarTraking();
	
	setTimeout(()=>{
		inicarTraking();
	},4000)
	
}

let enviar_mensaje = function (datos) {
	let tab = datos.tab
	chrome.tabs.sendMessage(tab.id, {
		command: datos.comando,
		title: datos.titulo
	},
	function(informe) {
		if(informe == undefined){
			enviar_mensaje(datos)
		}else{
			metodo_post(informe,tab);
		}
	});
}



chrome.runtime.onMessage.addListener((data, sender, callback) => {
	if(data.comando == 'iniciar') {
		chrome.tabs.getSelected( (tab) => {
			injectar_script(tab);
			iniciado = true;
			interval = setInterval(()=>{
				chrome.tabs.executeScript( tab.id, { code : 'window.location.reload();' });
			},600000)
		})
	}else if (data.comando == 'detener') {
		iniciado = false
		clearInterval(interval)
	}
})

chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
	if ( changeInfo.status == 'complete' ) {
		if(tab.url.indexOf('live') != -1){
			if(iniciado){
				injectar_script(tab);
			}
		}
	}
})

let sendMessage = (data) => {
	chrome.runtime.sendMessage(data, (x)=> {
		console.warn('popup_fail', x);
	});
}

chrome.browserAction.onClicked.addListener(function(tab) {
	let comando = inciado ? 'iniciado' : 'no-iniciado'
	console.log('comando enviado', comando)
	sendMessage({
		comando
	})
});