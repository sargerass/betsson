let popup = {
  ready: () => {
    $('#checkPage').on('click', function () {
      console.log("activo");
      if(!popup.toggle) {
        // $(this).removeClass('btn-inactive')
        // $(this).addClass('btn-active')
        // $(this).html('Desactivar')
        popup.toggle = true
        popup.sendMessage({
          comando: 'iniciar'
        })
      }else {
        // $(this).removeClass('btn-active')
        // $(this).addClass('btn-inactive')
        // $(this).html('Activar')
        popup.toggle = false
        popup.sendMessage({
          comando: 'detener'
        })
      }
    })
  },
  sendMessage: (data) => {
    chrome.runtime.sendMessage(data, (x)=> {
      console.warn('popup_fail', x);
    });
  },
  toggle: false
}

$(popup.ready)

chrome.runtime.onMessage.addListener((data, sender, callback) => {
	if(data.comando == 'iniciado') {
		chrome.tabs.getSelected( (tab) => {
      $(this).removeClass('btn-inactive')
      $(this).addClass('btn-active')
      $(this).html('Desactivar')
      popup.toggle = true
		})
	} else {
    $(this).removeClass('btn-active')
    $(this).addClass('btn-inactive')
    $(this).html('Activar')
    popup.toggle = false
  }
})